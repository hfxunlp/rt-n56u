# README #

This project is a fork of Padavan's firmware with various custom features I've found useful for myself, and other fixes merged in here and there. 

I am not a professional firmware developer. I used this firmware for my own purposes, but I'm not equipped to do full support and debugging of problems. Everything from this site is provided as-is. Using the tools and firmware provided in this repository can brick your router, or cause it to not work in ways I can neither predict or explain. You use this software at your own risk. You've been warned.

This firmware should be considered in "maintenance only" mode - I'll import fixes from other sources as they become available and update packages as appropriate, but generally speaking I have no plans to actively develop new features. I'm willing to add new hardware configurations if board files are provided.

### How do I get set up? ###

* [Get the tools to build the system](https://bitbucket.org/padavan/rt-n56u/wiki/EN/HowToMakeFirmware) or [Download pre-built system image](https://bitbucket.org/jeff_wischkaemper/rt-n56u/downloads)
* Feed the device with the system image file (Follow instructions of updating your current system)
* Perform factory reset
* Open web browser on http://my.router to configure the services

### Contribution guidelines ###

* All contributions are welcome. Please submit a pull request and I'll try to get to it as quickly as possible.
* If you'd like to take a more active role in development, please let me know. 